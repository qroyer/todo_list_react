import React, {useEffect} from 'react';
import {useState} from 'react';
import TaskListItem from '../Component/TaskListItem'


function TaskList({filter, isChecked, todoList, callback}) {

    let TasksToDisplay

    let i = -1;

    if (isChecked !== undefined) {
        TasksToDisplay = todoList
            .filter(({checked}) => checked === isChecked)
            .filter(({name}) => filter ? name.includes(filter) : true)
            .map(({id, checked, name, description, date}) => {
                i++;
                return <TaskListItem checked={checked} key={i} id={i} name={name} description={description}
                                     date={date} todoList={todoList} callback={callback}/>
            });
    } else {
        TasksToDisplay = todoList
            .filter(({name}) => filter ? name.includes(filter) : true)
            .map(({id, checked, name, description, date}) => {
                i++;
                return <TaskListItem checked={checked} key={i} id={i} name={name} description={description}
                                     date={date} todoList={todoList} callback={callback}/>
            });
    }

    return (
        <ul className="tasks mb-3">
            {
                TasksToDisplay
            }
        </ul>
    );
}

export default TaskList;
