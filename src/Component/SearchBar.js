import {useEffect, useState} from 'react';
import '../css/search.css';

function SearchBar(props) {
    const [query, setQuery] = useState([]);

    return (
        <div className="search-block p-4 mx-auto">
            <input className="mr-3 p-2 border-blue border-solid border-2" placeholder="Chercher une tâche" onChange={
                (event) => {
                    setQuery(event.target.value)
                    props.callback(query)
                }
            }/>
            <button className="btn bg-blue p-2 text-transparent	" onClick={() => props.callback(query)}>Search</button>
        </div>
    );
}

export default SearchBar;
