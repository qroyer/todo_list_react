import '../css/task.css'
import {useNavigate} from 'react-router-dom';
import saveTodolistLocalStorage from "./../Helper/Data";

const TaskListItem = (props) => {
    const {id, name, description, date} = props;
    const navigate = useNavigate();

    return (
        <li className="task-item" data-task={id}>
            <input type="checkbox" checked={props.checked} onChange={() => {
                check(props)
            }}/>
            <span className="mx-2 w-64">{name}</span>
            <span className="mx-2 w-64">{description}</span>
            <span className="mx-2 w-64">{date}</span>
            <button className="btn bg-blue p-0.5 text-transparent m-1	" onClick={() => {
                navigate('/edit', {state: {id, name, description, date}})
            }}>Editer
            </button>
            <button className="btn bg-blue p-0.5 text-transparent	" onClick={() => {
                deleteTask(props)
            }}>Supprimer
            </button>
        </li>
    )
}

const check = (props) => {
    let myTasks = props.todoList.slice();
    myTasks[props.id].checked = !myTasks[props.id].checked;
    saveTodolistLocalStorage(myTasks);
    props.callback(myTasks);
}

const deleteTask = (props) => {
    let myTasks = props.todoList.slice();

    myTasks.splice(props.id, 1)

    saveTodolistLocalStorage(myTasks);
    props.callback(myTasks);
}

export default TaskListItem;