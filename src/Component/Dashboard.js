import SearchBar from './../Component/SearchBar'
import TaskList from "./TaskList";
import {useState} from "react";

const Dashboard = (props) => {
    const [countryFilter, setCountryFilter] = useState(null);

    return (
        <div className="search-block w-full">
            <h1 className="font-extrabold text-lg ml-20 my-5">Tableau de bord</h1>
            <SearchBar callback={setCountryFilter}/>
            <div className="flex flex-row">
                <div className="w-1/2">
                    <p className="my-2 font-bold">Nombre de tâches non complétées: <span className="text-blue">{count(false, props.todoList)}</span></p>
                    <TaskList filter={countryFilter} isChecked={false} todoList={props.todoList} callback={props.callback}/>
                </div>
                <div className="w-1/2">
                    <p className="my-2 font-bold">Nombre de tâches complétées: <span className="text-blue">{count(true, props.todoList)}</span></p>
                    <TaskList filter={countryFilter} isChecked={true} todoList={props.todoList} callback={props.callback}/>
                </div>
            </div>
        </div>
    )
}

const count = (isChecked, todoList) => {
    let test = todoList.filter(({checked}) => checked === isChecked).length;

    return test

}

export default Dashboard;