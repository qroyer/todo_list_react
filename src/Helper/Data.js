export default function saveTodolistLocalStorage(todolist) {
    if (todolist) {
        localStorage.setItem('tasks', JSON.stringify(todolist));
    }
}