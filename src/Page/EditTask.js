import './../css/add.css';
import {useEffect, useState} from "react";
import {useNavigate, useLocation} from 'react-router-dom';

const EditTask = (props) => {
    const [name, setName] = useState("");
    const [description, setDescription] = useState("");
    const [date, setDate] = useState("");

    const navigate = useNavigate();
    const location = useLocation();


    return (
        <div className="edit-task-form">
            <h1>Modifier une tâche à ma todoList:</h1>
            <label>Nom de la tâche : </label>
            <input type="text" onChange={(e) => setName(e.target.value)} placeholder="Nom de la tâche"
                   defaultValue={location.state.name}/>
            <br/>
            <label>Description de la tâche : </label>
            <input type="text" onChange={(e) => setDescription(e.target.value)} placeholder="Description"
                   defaultValue={location.state.description}/>
            <br/>
            <label>Deadline : </label>
            <input type="date" onChange={(e) => setDate(e.target.value)} placeholder="Nom de la tâche"
                   defaultValue={location.state.date}/>
            <button onClick={() => {
                modifyTask(location.state, name, description, date)
                navigate('/tasks')
            }}>Sauvegarder
            </button>
        </div>
    )
}

const modifyTask = (state, checked, name, description, date) => {
    const myTask = {id: state.id, checked: false, name, description, date};

    myTask.name = name === '' ? state.name : name;
    myTask.checked = state.checked;
    myTask.description = description === '' ? state.description : description;
    myTask.date = date === '' ? state.date : date;

    localStorage.setItem('task' + state.id, JSON.stringify(myTask));

}

export default EditTask;