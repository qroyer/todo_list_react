import Dashboard from '../Component/Dashboard';

const Home = (props) => {

    return (
        <Dashboard todoList={props.todoList} callback={props.callback}/>
    )
}

export default Home;