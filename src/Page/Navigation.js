import {NavLink} from 'react-router-dom';
import './../css/nav.css';

const Navigation = () => {
    const links = [
        {to: "/", key: "dashboard", label: "Home"},
        {to: "/tasks", key: "tasks", label: "Tasks"},
        {to: "/addtask", key: "addtask", label: "Add Task"},
    ]

    return (
        <nav className={"p-6 max-w-g mx-auto bg-blue sticky shadow-lg flex justify-around font-roboto"}>
            {
                links.map(({to, key, label}) => {
                    return (
                        <NavLink to={to} key={key} className={({isActive}) => {
                            return isActive ? 'active' : null
                        }}>
                            {label}
                        </NavLink>
                    )
                })
            }
        </nav>
    )
}

export default Navigation;