import './../css/add.css';
import {useEffect, useState} from "react";
import saveTodolistLocalStorage from "./../Helper/Data";

let i = 0;

const AddTask = (props) => {
    const [name, setName] = useState("");
    const [description, setDescription] = useState("");
    const [date, setDate] = useState("");

    return (
        <div className="add-task-form ">
            <h1 className="font-extrabold text-lg ml-20 my-5">Ajouter une tâche à ma todoList:</h1>
            <label className="font-bold">Nom de la tâche : </label>
            <input className="mr-3 my-2 p-2 border-blue border-solid border-2" type="text" value={name} onChange={(e) => setName(e.target.value)} placeholder="Nom de la tâche"/>
            <br/>
            <label>Description de la tâche : </label>
            <input className="mr-3 my-2 p-2 border-blue border-solid border-2" type="text" value={description} onChange={(e) => setDescription(e.target.value)}
                   placeholder="Description"/>
            <br/>
            <label>Deadline : </label>
            <input className="mr-3 my-2 p-2 border-blue border-solid border-2" type="date" value={date} onChange={(e) => setDate(e.target.value)} placeholder="Nom de la tâche"/>
            <br/>
            <button className="ml-60 mt-1 btn bg-blue p-2 text-transparent	" onClick={() => {
                addItem(props, name, description, date)
            }}>Sauvegarder
            </button>
        </div>
    )
}

const addItem = (props, name, description, date) => {
    const myTask = {id: i, checked: false, name, description, date};
    props.todoList.push(myTask);
    saveTodolistLocalStorage(props.todoList)
    alert('Votre tâche a été ajoutée a votre Todo liste');
    i++;
}


export default AddTask;