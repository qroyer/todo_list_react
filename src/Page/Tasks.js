import SearchBar from './../Component/SearchBar'
import TaskList from './../Component/TaskList'
import {useState} from "react";

const Tasks = (props) => {
    const [countryFilter, setCountryFilter] = useState(null);

    return (
        <div className="search-block">
            <SearchBar callback={setCountryFilter}/>
            <TaskList filter={countryFilter} todoList={props.todoList} callback={props.callback}/>
        </div>
    )
}

export default Tasks;