import './App.css';
import Navigation from './Page/Navigation';
import Home from './Page/Home';
import {Routes, Route} from 'react-router-dom';
import Tasks from "./Page/Tasks";
import AddTask from "./Page/AddTask";
import EditTask from "./Page/EditTask";
import {useState} from "react";

function App() {
    const [todoList, setTodoList] = useState(loadTasksList());

    return (
        <div className="main-content">
            <Navigation/>
            <Routes>
                <Route path="/" element={<Home todoList={todoList} callback={setTodoList}/>}/>
                <Route path="/tasks" element={<Tasks todoList={todoList} callback={setTodoList}/>}/>
                <Route path="/addtask" element={<AddTask todoList={todoList} callback={setTodoList}/>}/>
                <Route path="/edit" element={<EditTask todoList={todoList} callback={setTodoList}/>}/>
            </Routes>
        </div>
    );
}

const loadTasksList = () => {
    let storageTodolist = localStorage.getItem('tasks')
    if (storageTodolist) {
        return JSON.parse(storageTodolist)
    } else {
        return []
    }
}

export default App;
