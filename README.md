# Projet TODO list React Quentin Royer

TODO liste fait avec React et j'ai utilisé Tailwind css en librairie css
- Features
- [x] Recherche des tâches (SearchBar)
- [x] Liste des tâches à effectuées
- [x] Stats du nombre de tâches à compléter et complétées
- [x] Ajout / suppression / édition de tâches
- [x] Check et uncheck d'une tâche
- [x] Persistance des données (dans le localstorage)
- [x] Librairie CSS externe utilisée : Tailwind css
- [x] Réutiliser les composants: SearchBar, TaskList, ...



- Dasboard
  - Recherche
  - Liste des tâches à effectuées
  - Liste des tâches complétées
  - Menu de navigation

- Liste des tâches
  - Liste de toutes les tâches
  - Recherche sur les tâches
- Ajout de tâches 
  - Nom, Description, Date

    
## Available Scripts

In the project directory, you can run:

### `npm start`

